#!/usr/bin/env python
from __future__ import print_function
from __future__ import division
import csv
import os
import sys
import numpy as np
import cv2
import time
import pytesseract

try:
    import Image
except ImportError:
    from PIL import Image

base_path = "text_extract/img"

pattern_threshold = 0.2  # threshold which is used for determining color pattern, 0.09 < threshold < 0.3
bound_threshold = 5


def angle_cos(p0, p1, p2):
    """
    get cosine value from given 3 points
    :param p0:
    :param p1:
    :param p2:
    :return:
    """
    d1, d2 = (p0 - p1).astype('float'), (p2 - p1).astype('float')
    return abs(np.dot(d1, d2) / np.sqrt(np.dot(d1, d1) * np.dot(d2, d2)))


def find_rect(img):
    """
    Find rectangles from the image
    :param img:
    :return: list of 4-point axis (x-axis, y-axis, width, height)
    """
    src_height, src_width, channels = img.shape
    src_area = src_height * src_width
    # print "Source image width " + str(src_width) + " height " + str(src_height) + " area " + str(src_area)

    # Form a border around an image
    bordered = cv2.copyMakeBorder(img, 1, 1, 1, 1, cv2.BORDER_CONSTANT)

    blured = cv2.GaussianBlur(bordered, (0, 0), 0.3)

    # Perform image Denoising using Non-local Means Denoising algorithm
    denoised = cv2.fastNlMeansDenoising(blured, None, 10)

    # Sharpen the image
    sharpened = cv2.addWeighted(denoised, 3, denoised, -1, 0)

    r_list = []
    for gray in cv2.split(sharpened):
        # Calculate histogram
        cv2.equalizeHist(gray, gray)

        # get rectangles from multiple edges
        for th in xrange(0, 255, 25):
            if th == 0:
                cannied = cv2.Canny(gray, 0, 50, apertureSize=5)
                _bin = cv2.dilate(cannied, None)
            else:
                retval, _bin = cv2.threshold(gray, th, 255, cv2.THRESH_BINARY)

            contoured, contours, hierarchy = cv2.findContours(_bin, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
            for cnt in contours:
                # detect polygon
                cnt = cv2.approxPolyDP(cnt, 0.01 * cv2.arcLength(cnt, True), True)
                # before - hard limit of 1300 pixels
                # polygon count must be 4, area should be greater than 1000, must be convex
                # if len(cnt) == 4 and cv2.contourArea(cnt) > 1300 and cv2.isContourConvex(cnt):
                # now checks to see if area is more than 2% of overall ad
                thres = 1300
                if src_area / 50 < thres:
                    thres = src_area / 50

                if len(cnt) == 4 and cv2.contourArea(cnt) != 0 and cv2.contourArea(cnt) > thres and cv2.isContourConvex(cnt):
                    cnt = cnt.reshape(-1, 2)
                    max_cos = np.max([angle_cos(cnt[i], cnt[(i + 1) % 4], cnt[(i + 2) % 4]) for i in range(4)])
                    if max_cos < 0.1:
                        r_list.append(cnt)
    return r_list


def create_dir(dir_path):
    """
    Create Directory if not exists
    :param dir_path:
    :return:
    """
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)


def remove_similar_rects(r_list):
    """
    - group similar items
    - get average value for each group, and save it to the list
    :param r_list: list of rectangles
    :return:    list of average values
    """
    sort_list = []
    r_list = list(set(r_list))
    r_list.sort()
    r_list.reverse()

    # group similar items
    inserted_list = []
    for i in range(0, len(r_list)):
        if i in inserted_list:
            continue
        tmp_list = [i, ]
        inserted_list.append(i)

        for j in range(i + 1, len(r_list)):
            if j in inserted_list:
                continue
            x1, y1, w1, h1 = r_list[i]
            x2, y2, w2, h2 = r_list[j]
            dist = abs(x1 - x2) + abs(y1 - y2) + abs(w1 - w1) + abs(h1 - h2)
            if dist < 50:
                tmp_list.append(j)
                inserted_list.append(j)

        sort_list.append(tmp_list)

    # get average value among the group
    result = []
    for s in sort_list:
        ax = []
        ay = []
        aw = []
        ah = []
        for k in s:
            ax.append(r_list[k][0])
            ay.append(r_list[k][1])
            aw.append(r_list[k][2])
            ah.append(r_list[k][3])

        # pop maximum/minimum values
        if len(ax) > 3:
            ax.sort()
            ay.sort()
            aw.sort()
            ah.sort()
            ax.pop(0)
            ay.pop(0)
            aw.pop(0)
            ah.pop(0)
            ax.pop(-1)
            ay.pop(-1)
            aw.pop(-1)
            ah.pop(-1)
        result.append([int(np.average(ax)), int(np.average(ay)), int(np.average(aw)), int(np.average(ah))])
    return result


def paint_with_bg(src_img, axis, dest_axis, number):
    """
    Remove foreground in the part of image given by axis(x, y, w, h)
    :param src_img: source image
    :param axis: area which is referred to get background color
    :param dest_axis: area which is needed to paint.
    :param number: referred number which is used in deciding background, see code below
    :return: removed image
    """
    img = src_img[axis[1]:axis[1] + axis[3], axis[0]:axis[0] + axis[2]]

    max_point = []
    # get max frequent color in the area
    for m in cv2.split(img):
        unique, counts = np.unique(m, return_counts=True)
        max_index = list(counts).index(max(counts))
        max_val = unique[max_index]

        if number == 2 and unique.size > 1:  # get the secondly frequent value
            # pop maximum value
            new_counts = np.delete(counts, max_index)
            new_unique = np.delete(unique, max_index)
            max_index = list(new_counts).index((max(new_counts)))
            max_val = new_unique[max_index]
        max_point.append(max_val)

    # paint destination area with ths most frequent color
    for j in range(3):
        src_img[dest_axis[1]:dest_axis[1] + dest_axis[3], dest_axis[0]:dest_axis[0] + dest_axis[2], j] = max_point[j]

    return src_img


def sortkey(a):
    """
    callback of sort function
    :param a: item of list which is needed to sort
    :return: returns the final value - the sorting key is the final value
    """
    return a[-1]


def remove_boxes(src_img, re_list):
    """
    Remove found boxes and save to file
    NOTE:  We need to sort rectangles to be sorted with their area.
           And then, while removing characters from the small rectangle, all characters in the boxes are removed
           After that, decide whether to remove box itself and remove it.

    :param src_img: source image
    :param re_list: list of 4-axis rectangles which was found before
    :return:
    """
    # cv2.imshow('Original Image', src_img)
    # append area to each rectangle for sorting
    for r in re_list:
        r.append(r[2] * r[3])
    # sort rectangles with their area size
    re_list.sort(key=sortkey)

    # at first, remove characters in the found boxes
    for r in re_list:
        r.pop(-1)  # pop area
        src_img = paint_with_bg(src_img, r, r, 1)

    # cv2.imshow('Removed foreground', src_img)

    # if every edge of box is not near to the edge of whole image, remove box itself
    thr = bound_threshold  # boundary of box : get color of box's boundary and fill box with this color
    for r in re_list:
        hei = np.size(image, 0)
        wid = np.size(image, 1)
        x, y, w, h = r
        if x > thr and wid - x - w > thr and y > thr and hei - y - h > thr:
            bg_area = [x - thr, y - thr, w + 2 * thr, h + 2 * thr]
            dest_area = [x - 1, y - 1, w + 2, h + 2]
            src_img = paint_with_bg(src_img, bg_area, dest_area, 2)

    # cv2.imshow('Without Boxes', src_img)

    return src_img


def get_inner_freq(img_src, rr):
    """
    Get unique count in the area of image
    If this value is high, this area will be texture, otherwise will have simple color pattern
    :param img_src:
    :param rr:
    :return:
    """
    x, y, w, h = rr
    tmp_img = img_src[y:y + h, x:x + w]
    cnt = 0.0
    # get total count of different colors in each channel
    for m in cv2.split(tmp_img):
        unique, counts = np.unique(m, return_counts=True)
        max_cnt = max(counts)
        cnt += max_cnt / w / h

    # print("Color count: ", cnt / 3.0)
    return cnt / 3.0


def get_outer_freg(img_src, rr):
    """
    Get unique count in the boundary of area.
    :param img_src:
    :param rr:
    :return:
    """
    x, y, w, h = rr
    thr = bound_threshold
    # get square measure of boundary area
    hei = np.size(image, 0)
    wid = np.size(image, 1)
    if x > thr and wid - x - w > thr and y > thr and hei - y - h > thr:
        cnt = 0.0
        # get total count of different colors in each channel
        # top side
        tmp_img = img_src[y - thr:y, x:x + w + thr]
        sq_meas = thr * (w + thr)
        for mm in cv2.split(tmp_img):
            unique, counts = np.unique(mm, return_counts=True)
            max_cnt = max(counts)
            cnt += max_cnt / sq_meas

        # right side
        sq_meas = thr * (h + thr)
        tmp_img = img_src[y:y+h+thr, x+w:x+w+thr]
        for mm in cv2.split(tmp_img):
            unique, counts = np.unique(mm, return_counts=True)
            max_cnt = max(counts)
            cnt += max_cnt / sq_meas

        # bottom side
        sq_meas = thr * (w + thr)
        tmp_img = img_src[y+h:y+h+thr, x-thr:x+w]
        for mm in cv2.split(tmp_img):
            unique, counts = np.unique(mm, return_counts=True)
            max_cnt = max(counts)
            cnt += max_cnt / sq_meas

        # left side
        tmp_img = img_src[y - thr:y + h, x - thr:x]
        sq_meas = thr * (h + thr)
        for mm in cv2.split(tmp_img):
            unique, counts = np.unique(mm, return_counts=True)
            max_cnt = max(counts)
            cnt += max_cnt / sq_meas
        return cnt / 12.0

    else:
        print (rr, "  No boundary")
        return 0


# sorted by y, then X
def arrange_boxes(rr):
    return sorted(rr, key=lambda xx: (xx[1], xx[0]))


def remove_outer_boxes(rr):
    """
    Remove outer boxes(contains any other box)
    :param rr:
    :return:
    """
    # append area to each rectangle for sorting
    for r in rr:
        r.append(r[2] * r[3])
    # sort rectangles with their area size
    rr.sort(key=sortkey)
    rr.reverse()
    remove_list = []

    for r in rr:
        for jj in range(rr.index(r)+1, len(rr)):
            if r[0] <= rr[jj][0] and r[1] <= rr[jj][1] and r[0]+r[2] >= rr[jj][0]+rr[jj][2] and r[1]+r[3] >= rr[jj][1]+rr[jj][3]:
                remove_list.append(r)
                break

    for r in remove_list:
        rr.remove(r)

    for r in rr:
        r.pop(-1)

    return rr


def extract_text(file_name, csv_path, orig_image):
    """
    Extract text lines from the given image and save metadata to csv file.
    :param file_name:  Image file name
    :param csv_path: path of result csv file.
    :param orig_image: original image
    :return:
    """
    img_init = cv2.imread(file_name)

    img_final = cv2.imread(file_name)
    img2gray = cv2.cvtColor(img_init, cv2.COLOR_BGR2GRAY)

    ret, mask = cv2.threshold(img2gray, 180, 255, cv2.THRESH_BINARY)
    image_final = cv2.bitwise_and(img2gray, img2gray, mask=mask)
    ret, new_img = cv2.threshold(image_final, 180, 255, cv2.THRESH_BINARY)  # for black text, cv.THRESH_BINARY_INV

    # to manipulate the orientation of dilution,
    # large x means horizonatally dilating more, large y means vertically dilating more
    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))

    dilated = cv2.dilate(new_img, kernel, iterations=6)  # dilate , more the iteration more the dilation

    contoured, contours, hierarchy = cv2.findContours(dilated, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)  # get contours
    index = 0
    if contours is not None:
        with open(csv_path, 'wb') as csv_text:
            writer_text = csv.writer(csv_text, delimiter=',')
            for contour in contours:
                buf = ['TextLine' + str(index), ]
                # get rectangle bounding contour
                [x, y, w, h] = cv2.boundingRect(contour)

                # Don't plot small false positives that aren't text
                if w < 35 and h < 35:
                    continue

                # draw rectangle around contour on original image
                cv2.rectangle(img_init, (x, y), (x+w, y+h), (255, 0, 255), 2)

                # save cropped image and extract text
                cropped = img_final[y:y + h, x: x + w]
                s = os.path.dirname(os.path.dirname(file_name)) + '/crop_' + str(index) + '.jpg'
                cv2.imwrite(s, cropped)

                # extract text from the saved image file
                txt_img = Image.open(s)
                line_text = pytesseract.image_to_string(txt_img)

                buf += [x, y, w, h, line_text]

                n = get_inner_freq(orig_image, (x, y, w, h))
                if n < pattern_threshold:
                    buf.append('Box texture')
                else:
                    buf.append('Box plain')

                m = get_outer_freg(orig_image, (x, y, w, h))

                if m == 0:
                    buf.append('Surround none')
                elif m < pattern_threshold:
                    buf.append('Surround texture')
                else:
                    buf.append('Surround plain')

                writer_text.writerow(buf)

                print("LINE: ", line_text)
                os.remove(s)
                index += 1

    # write original image with added contours to disk
    # cv2.imshow('text_detect_result', img_init)
    # cv2.waitKey()


if __name__ == '__main__':

    extentions = ['jpg', 'png']
    file_names = [fn for fn in os.listdir(base_path) if any(fn.endswith(ext) for ext in extentions)]

    create_dir(base_path + '/output/box_removed/')

    # file_names = ['bell-id-half-page.png', ]
    # file_names = ['appinsider.png', ]
    for fn in file_names:
        s_time = time.time()
        f_name = base_path + "/" + fn
        image = cv2.imread(f_name)
        height = np.size(image, 0)
        width = np.size(image, 1)
        rects = find_rect(image)

        # convert np style to axis data
        rect_list = []
        for rect in rects:
            x, y, w, h = cv2.boundingRect(rect)
            if width + height > w + h + 10:   # remove full rect
                rect_list.append((x-1, y-1, w, h))  # minus 1 since we have added border to the image before

        rect_list = remove_similar_rects(rect_list)

        rect_list = remove_outer_boxes(rect_list)

        rect_list = arrange_boxes(rect_list)

        # draw rectangles on the image
        # for rect in rect_list:
        #     x, y, w, h = rect
        #     cv2.imshow('Box-'+str(rect_list.index(rect)), image[y:y+h, x:x+w])
        copied_img = image.copy()
        for rect in rect_list:
            x, y, w, h = rect
            cv2.rectangle(copied_img, (x, y), (x+w, y+h), (0, 255, 0), 2)

        # cv2.imshow(fn, copied_img)

        # produce result
        path = base_path + '/output/' + fn.split('.')[0]
        create_dir(path)

        f_name_text = f_name

        if len(rect_list) > 0:
            with open(path + '/result.csv', 'wb') as csvfile:
                writer = csv.writer(csvfile, delimiter=',')
                for i in range(len(rect_list)):
                    box_name = 'Box' + str(i+1)
                    # write each boxed to png file
                    x, y, w, h = rect_list[i][:4]

                    create_dir(path + "/" + box_name)
                    box_file_name = path + "/" + box_name + "/" + box_name + ".png"
                    crop_img = image[y:y+h, x:x+w]
                    cv2.imwrite(box_file_name, crop_img)

                    # save box's axis to CSV file
                    tmp = list(rect_list[i])
                    tmp.insert(0, box_name)
                    n = get_inner_freq(image, rect_list[i])
                    if n < pattern_threshold:
                        tmp.append('Box texture')
                    else:
                        tmp.append('Box plain')

                    m = get_outer_freg(image, rect_list[i])

                    if m == 0:
                        tmp.append('Surround none')
                    elif m < pattern_threshold:
                        tmp.append('Surround texture')
                    else:
                        tmp.append('Surround plain')
                    writer.writerow(tmp)

                    # print(rect_list[i], " ", n, " ", m)

                print("Found: %d" % len(rect_list))

            box_removed = remove_boxes(image.copy(), rect_list)
            # result_name = path + "/B1-1.png"
            result_name = base_path + '/output/box_removed/' + fn.split(".")[0] + "_removed_box.jpg"
            cv2.imwrite(result_name, box_removed)

            f_name_text = result_name

        else:
            result_name = base_path + '/output/box_removed/' + fn.split(".")[0] + "_removed_box.jpg"
            cv2.imwrite(result_name, image)

        print("Elapsed: ", time.time() - s_time)
        # cv2.waitKey(0)

    cv2.destroyAllWindows()
