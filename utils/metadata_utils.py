import sys, os
import csv
import argparse
import json
import inspect

# definitive metadata structure

# name, x, y, w, h, object_texture, surround_texture, text, isCta, isSlide
# 0     1  2  3  4  5               6                7     8      9 


class Feature:
  def __init__(self, x,y,w,h,name=False, object_texture=False,surround_texture=False, text=False, is_cta=False,is_slide = False, file_path=False, rel_path=False):
    self.x = x
    self.y = y
    self.w = w
    self.h = h
    self.name = name
    self.object_texture = object_texture
    self.surround_texture = surround_texture
    self.text = text
    self.is_cta = is_cta
    self.is_slide = is_slide
    self.file_path = file_path
    self.rel_path = rel_path


  def is_truthy_value(self, value, truth_text):
    if value == truth_text:
      return True
    elif value == "True":
      return True
    elif value == True:
      return True
    else:
      return False

  def xp(self, src_width):
    return self.horizontal_pixel_to_percentage(self.x, src_width)

  def yp(self, src_height):
    return self.vertical_pixel_to_percentage(self.y, src_height)

  def wp(self, src_width):
    return self.horizontal_pixel_to_percentage(self.w, src_width)

  def hp(self, src_height):
    return self.vertical_pixel_to_percentage(self.h, src_height)

  def horizontal_pixel_to_percentage(self, value, src_width):
    return float( (value/float(src_width)) * 100 )

  def vertical_pixel_to_percentage(self, value, src_height):
    return float( (value/float(src_height)) * 100 )

  def pixel_to_percentage(self, src_width, src_height):
    px,py,pw,ph = ( [self.x/float(src_width),self.y/float(src_height),self.w/float(src_width),self.h/float(src_height)])
    #print ("  X " + str(x) + " Y " + str(y) + " W " + str(w) + " H " + str(h) + " as a fraction of src_width/src_height ") + str(src_height) + "/" + str(src_width);
    px,py,pw,ph = ( [float(px*100),float(py*100),float(pw*100),float(ph*100)])

    return px,py,pw,ph


  def type_string(self):
    if self.is_Cta():
      return "CTA"
    elif self.is_Slide():
      return "Slide"
    elif self.is_Textline():
      return "TextLine"
    else:
      return "Object"

  def is_Cta(self):
    return self.is_truthy_value(self.is_cta, "isCta")

  def is_Slide(self):
    return self.is_truthy_value(self.is_slide, "isSlide")

  def has_text(self):
    return self.is_truthy_value(self.text, "")

  def is_Textline(self):
    return self.name.startswith("TextLine") 

  def to_JSON(self):
    return json.dumps(self, default=lambda o: o.__dict__, 
      sort_keys=True, indent=4)

  def to_row(self):
    row = []
    row.append(self.name)
    row.append(self.x)
    row.append(self.y)
    row.append(self.w)
    row.append(self.h)
    row.append(self.object_texture)
    row.append(self.surround_texture)
    row.append(self.text)
    row.append(self.is_Cta())
    row.append(self.is_Slide())
    row.append(self.file_path)
    row.append(self.rel_path)

    return row


  def __str__(self):
    info = "name: " + self.name
    info = info + " x: " + str(self.x) + " y: " + str(self.y) + " w: " + str(self.w) + " h: " + str(self.h) 
    info = info + " is textured " + str(self.object_texture)
    info = info + " is surrounded by " + str(self.surround_texture)
    info = info + " is Cta " + str(self.is_Cta())
    info = info + " is Slide  " + str(self.is_Slide())
    info = info + " file path is  " + str(self.file_path)
    info = info + " relative path is  " + str(self.rel_path)
    info = info + " has text '" + str(self.text)+ "'"

    return info

#
# write the latest version
# 
def save_to_csv(file_name, features):
    curframe = inspect.currentframe()
    calframe = inspect.getouterframes(curframe, 2)
    print 'Save to CSV called by:', calframe[1][3]

    with open(file_name,'wb') as wcsvfile:
      writer = csv.writer(wcsvfile, delimiter=',')
      for feature in features:
        writer.writerow(feature.to_row())

    wcsvfile.close()

#
# load the legacy format
# 
def load_from_csv(filename):
  if os.access(filename, os.F_OK) == False:
    print "No CSV file, exiting file: " + filename
    return []

  features = []
  with open(filename, 'rb') as rcsvfile:
    reader = csv.reader(rcsvfile, delimiter=',')
    info_shown = False
    known_format = False
    for row in reader:
      if not info_shown:
        print str(len(row)) + " elements"
        print "first row"
        print ', '.join(row)
        info_shown = True

#      if (len(row) == 5):
#        known_format = True
#        x,y,w,h,isCta,isSlide = [int(row[1]),int(row[2]),int(row[3]),int(row[4]),str(row[5]),str(row[6])]
#        f = Feature(x,y,w,h)
#        f.is_cta = isCta
#        f.is_slide = isSlide
#        features.append(f)

#      if (len(row) == 7):
#        known_format = True
#        name,x,y,w,h,isCta,isSlide = [row[0],int(row[1]),int(row[2]),int(row[3]),int(row[4]),str(row[5]),str(row[6])]
#        f = Feature(x,y,w,h)
#        f.name = name
#        f.is_slide = isSlide
#        f.is_cta = isCta
#        features.append(f)

#      if (len(row) == 10):
#        known_format = True
#        name,x,y,w,h,object_texture, surround_texture, text, isCta,isSlide = [row[0],int(row[1]),int(row[2]),int(row[3]),int(row[4]),str(row[5]),str(row[6]), str(row[7]), str(row[8]), str(row[9])]
#        f = Feature(x,y,w,h)
#        f.name = name
#        f.object_texture = object_texture
#        f.surround_texture = surround_texture
#        f.text = text
#        f.is_cta = isCta
#        f.is_slide = isSlide
#        features.append(f)

      if (len(row) == 12):
        known_format = True
        name,x,y,w,h,object_texture, surround_texture, text, isCta,isSlide, file_path, rel_path = [row[0],int(row[1]),int(row[2]),int(row[3]),int(row[4]),str(row[5]),str(row[6]), str(row[7]), str(row[8]), str(row[9]), str(row[10]),str(row[11]) ]
        f = Feature(x,y,w,h)
        f.name = name
        f.object_texture = object_texture
        f.surround_texture = surround_texture
        f.text = text
        f.is_cta = isCta
        f.is_slide = isSlide
        f.file_path = file_path
        f.rel_path = rel_path
        features.append(f)
      if not known_format:
        print "ERROR unknown CSV format"
   
  rcsvfile.close()

  return features

# the file name is not the same as the web
# relative path.  The method poplates
# the relative path (rel_path) property of 
# features so it can be used in templates
def set_rel_path(features, output_dir):
  for feature in features:
    fp = feature.file_path
    rp = fp[(len(output_dir)):]
    feature.rel_path=rp

def convert_pixels_to_percent(features, src_width, src_height):
  for feature in features:
    x,y,w,h = feature.pixel_to_percentage(src_width, src_height)
    feature.x = x
    feature.y = y
    feature.w = w
    feature.x = h

  return features


def classify_features(features):
  pure_objects = []
  slides = []
  ctas = []
  textlines = []
  
  for feature in features:
    if feature.is_Cta():
      ctas.append(feature)
    elif feature.is_Slide():
      slides.append(feature)
    elif feature.is_Textline():
      textlines.append(feature)
    else:
      pure_objects.append(feature)

  return pure_objects, slides, ctas, textlines



def convert_features_to_template_objects(features, src_width, src_height):
  pure_objects = []
  slides = []
  ctas = []
  textlines = []
  
  for feature in features:
    x,y,w,h = feature.pixel_to_percentage(src_width, src_height)

    if feature.is_Cta():
      ctas.append( [x,y,w,h] )
    elif feature.is_Slide():
      slides.append( [x,y,w,h] )
    elif feature.is_Textline():
      textlines.append( [x,y,w,h] )
    else:
      pure_objects.append([x,y,w,h] )

  return pure_objects, slides, ctas, textlines

def features_json(features):
  for feature in features:
    print feature.to_JSON()

def features_report(features):
  print "Features report"
  print "# Features " + str(len(features))
  for feature in features:
    print feature
  

if __name__ == '__main__':

  print "name, x, y, w, h, object_texture, surround, texture, text, isCta, isSlide, file_path, rel_path"
  parser = argparse.ArgumentParser()
  parser.add_argument("-i", "--input_file_name", required=True, help="metadata.csv file")
  parser.add_argument("-o", "--output_file_name", required=False, help="Output metadata file")

  inst_arg = parser.parse_args()
  args = vars(inst_arg)

  features = load_from_csv(args['input_file_name'])

#  features_report(features)
  print "converting to percentages of 300,250"
#  convert_pixels_to_percent(features, 300,250) 
  features_report(features)
#  features_json(features)

  if args['output_file_name']:
    save_to_csv(args['output_file_name'], features)


